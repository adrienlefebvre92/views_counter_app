# views_counter_app

Ensure server is running on ```http://localhost:3000```


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
Then go on ````http://localhost:8080````

![Alt text](static/capture.png)
